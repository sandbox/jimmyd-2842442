(function ($) {
    $(document).ready(function () {

        $('.hide_rate_field').each(function() {

        var formElement =  "<fieldset id='rateField" +this.id + "' class='rating'>";
            formElement += "<input class='stars' type='radio' id='star50" +this.id + "' name='rating' value='10' />";
            formElement += "<label class = 'full' for='star50" +this.id + "' title='Awesome - 5 stars'></label>";
            formElement += "<input class='stars' type='radio' id='star45" +this.id + "' name='rating' value='9' />";
            formElement += "<label class='half' for='star45" +this.id + "' title='Pretty good - 4.5 stars'></label>";
            formElement += "<input class='stars' type='radio' id='star40" +this.id + "' name='rating' value='8' />";
            formElement += "<label class = 'full' for='star40" +this.id + "' title='Pretty good - 4 stars'></label>";
            formElement += "<input class='stars' type='radio' id='star35" +this.id + "' name='rating' value='7' />";
            formElement += "<label class='half' for='star35" +this.id + "' title='Meh - 3.5 stars'></label>";
            formElement += "<input class='stars' type='radio' id='star30" +this.id + "' name='rating' value='6' />";
            formElement += "<label class = 'full' for='star30" +this.id + "' title='Meh - 3 stars'></label>";
            formElement += "<input class='stars' type='radio' id='star25" +this.id + "' name='rating' value='5' />";
            formElement += "<label class='half' for='star25" +this.id + "' title='Kinda bad - 2.5 stars'></label>";
            formElement += "<input class='stars' type='radio' id='star20" +this.id + "' name='rating' value='4' />";
            formElement += "<label class = 'full' for='star20" +this.id + "' title='Kinda bad - 2 stars'></label>";
            formElement += "<input class='stars' type='radio' id='star15" +this.id + "' name='rating' value='3' />";
            formElement += "<label class='half' for='star15" +this.id + "' title='Meh - 1.5 stars'></label>";
            formElement += "<input class='stars' type='radio' id='star10" +this.id + "' name='rating' value='2' />";
            formElement += "<label class = 'full' for='star10" +this.id + "' title='Sucks big time - 1 star'></label>";
            formElement += "<input class='stars' type='radio' id='star05" +this.id + "' name='rating' value='1' />";
            formElement += "<label class='half' for='star05" +this.id + "' title='Sucks big time - 0.5 stars'></label>";
            formElement += "</fieldset><br />";

            $("#" + this.id).before(formElement);

            $("#rateField" + this.id +" .stars").click(function () {

                $("#" + this.id.substring(6)).val($(this).val());
                $(this).attr("checked");
            });

        });

    });

}(jQuery));