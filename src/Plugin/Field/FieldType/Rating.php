<?php

/**
 * @file
 * Contains \Drupal\RateField\Plugin\Field\FieldType\Rating.
 */

namespace Drupal\rate_field\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'Rating' field type.
 *
 * @FieldType(
 *   id = "rating",
 *   label = @Translation("Rating field"),
 *   description = @Translation("This field stores a rating of the object."),
 *   category = @Translation("General"),
 *   default_widget = "rating_default",
 *   default_formatter = "rating_stars"
 * )
 */
class Rating extends FieldItemBase {
  /**
   * {@inheritdoc}
   */
  public static function schema(\Drupal\Core\Field\FieldStorageDefinitionInterface $field_definition) {
    return array(
      'columns' => array(
        'rating' => array(
          'description' => 'Rating',
          'type' => 'int',
          'length' => '255',
          'not null' => TRUE,
          'default' => '0',
        ),
      ),
      'indexes' => array(
        'rating' => array('rating'),
      ),
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['rating'] = \Drupal\Core\TypedData\DataDefinition::create('integer')
      ->setLabel(t('rating'));

    return $properties;
  }

}
