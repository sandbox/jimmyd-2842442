<?php

/**
 * @file
 * Contains \Drupal\rate_field\Plugin\Field\FieldWidget\RatingDefaultWidget
 */

namespace Drupal\rate_field\Plugin\Field\FieldWidget;

use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'rating_default' widget.
 *
 * @FieldWidget(
 *   id = "rating_default",
 *   label = @Translation("Rating"),
 *   field_types = {
 *     "number"
 *   }
 * )
 */
class RatingDefaultWidget extends WidgetBase {
  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $element['rating'] = array(
      '#type' => 'number',
      '#title' => t('Rating'),
      '#min' => 0,
      '#max' => 10,
      '#default_value' => 0,
      '#required' => $element['#required'],
      '#attached' => array(
        'library' => array(
          'core/jquery',
          'rate_field/admin-star-field',
        ),
      ),
    );
    return $element;
  }

}
