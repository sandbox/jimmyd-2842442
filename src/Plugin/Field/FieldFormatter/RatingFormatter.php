<?php

/**
 * @file
 * Contains \Drupal\rate_field\Plugin\Field\FieldFormatter\RatingFormatter
 */

namespace Drupal\rate_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'rating_stars' formatter.
 *
 * @FieldFormatter(
 *   id = "rating_stars",
 *   label = @Translation("Stars view of rating field"),
 *   field_types = {
 *     "rating"
 *   }
 * )
 */

class RatingFormatter extends FormatterBase {
  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    foreach ($items as $delta => $item) {
    $total = $item->rating/2;

    $stars = '';

    //Place colored stars
    for($i =0; $i < floor($total); $i++)
    {
        $stars .= '<span class="star on"></span>';
    }

    //Place half star
    if(floor($total) != $total)
    {
       $stars .= '<span class="star half"></span>';
    }

    //Place open stars
    for($i = ceil($total); $i < 5; $i++)
    {
        $stars .= '<span class="star"></span>';
    }

      $element[$delta] = array(
        '#markup' => $stars,
        '#attached' => array(
        'library' =>  array(
          'rate_field/star-field'
        ),
      ),
      );
    }
    return $element;
  }
}
